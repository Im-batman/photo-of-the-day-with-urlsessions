//
//  PhotoInfo.swift
//  spacePhoto
//
//  Created by Asar Sunny on 09/11/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import Foundation

struct PhotoInfo:Codable{
    
    var title: String
    var description : String
    var url : URL
    var copyright:String?
    
 
    
    enum keys:String, CodingKey{
        case title
        case description = "explanation"
        case url
        case copyright
        
        
    }
    
    
    init(from decoder: Decoder) throws {
        let valueContainer = try decoder.container(keyedBy:keys.self)
        self.title = try valueContainer.decode(String.self,forKey: keys.title)
        self.description = try valueContainer.decode(String.self,forKey: keys.description)
        self.url = try valueContainer.decode(URL.self, forKey:keys.url)
        self.copyright = try valueContainer.decode(String.self, forKey: keys.copyright)
    }
    
    
    
    
    
    
    
    
}
