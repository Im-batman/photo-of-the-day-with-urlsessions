//
//  PhotoInfoController.swift
//  spacePhoto
//
//  Created by Asar Sunny on 09/11/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import Foundation

struct PhotoInfoController{
    
    
    
    static func fetchPhotoInfo(_ params:[String:String],Completion:@escaping (PhotoInfo?)->Void){
//        https://api.nasa.gov/planetary/apod
        
        
        
        let queries = params
        let baseUrl = URL(string: "https://api.nasa.gov/planetary/apod")!
        let url = baseUrl.withQueries(queries)!
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            let jsonDecoder = JSONDecoder()
            if let data = data,
                let json = try? jsonDecoder.decode(PhotoInfo.self, from: data){
                
                Completion(json)
            }else{
                print("something horrible happend ......")
                Completion(nil)
            }
        }
        
        task.resume()
        
    }
    
    
}
