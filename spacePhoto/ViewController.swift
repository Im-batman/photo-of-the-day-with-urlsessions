//
//  ViewController.swift
//  spacePhoto
//
//  Created by Asar Sunny on 09/11/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
let photoInfoController = PhotoInfoController()
    
    let query: [String: String] = [
        "api_key": "DEMO_KEY",
//        "date": "2018-07-13"
        ]
    
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var copyright: UILabel!
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    
    @IBOutlet weak var loadingLabel: UILabel!
    @IBOutlet weak var stackview: UIStackView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        indicator.isHidden = false
        indicator.startAnimating()
        
        // Do any additional setup after loading the view, typically from a nib.
        
        descriptionLabel.text = ""
        copyright.text = ""
        
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        PhotoInfoController.fetchPhotoInfo(query) { (photoData) in
            if let photoData = photoData{
                print(photoData)
                self.updateUI(with: photoData)
                
            }
        }
        
       
    }
    
    
    
    func updateUI(with photoInfo:PhotoInfo){
        let photoData = photoInfo
        
        let task = URLSession.shared.dataTask(with: photoInfo.url) { (data, response, error) in
            if let data = data {
               
                if let image = UIImage(data: data){
                    DispatchQueue.main.async {
                        self.indicator.stopAnimating()
                        self.indicator.isHidden = true
                        self.loadingLabel.isHidden = true
                        self.stackview.isHidden = true
                        
                        self.imageView.image = image
                        self.title = photoData.title
                        self.descriptionLabel.text = photoData.description
                        if let copyright = photoData.copyright{
                            self.copyright.text = "copyRight \(copyright)"
                        }else{
                            self.copyright.isHidden = true
                        }
                        
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        
                    }
                    
                    
                    
                }
            }
        }
        
        task.resume()
        
        
        
        
    }
    
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

