//
//  urlHelper.swift
//  spacePhoto
//
//  Created by Asar Sunny on 09/11/2018.
//  Copyright © 2018 Gurudeveloperinc. All rights reserved.
//

import Foundation


extension URL {
    func withQueries(_ queries:[String:String])->URL?{
        var component = URLComponents(url: self, resolvingAgainstBaseURL: true)
        component?.queryItems = queries.map{
            URLQueryItem(name: $0.0, value: $0.1)
    }
    return component?.url
    
    }
}
